
def cmp_lists(list1, list2):
	if len(list1) != len(list2): return False
	for i, e in enumerate(list1):
		if e != list2[i]: return False
	return True

cells = [0] * 101
cells[0] = 42000
cells[100] = 42

done = False
iteration = 0

while not done:
	iteration+=1
	cells2 = [0] * 101
	cells2[0] = 42000
	cells2[100] = 42
	for i in range(1,100):
		cells2[i] = int((1.0/3) * (cells[i-1] + cells[i] + cells[i+1]))
	if cmp_lists(cells, cells2) is True:
		done = True
	cells = cells2
	print("({}) ".format(iteration), end='', flush=True)
	for cell in cells:
		print("{} ".format(cell), end='', flush=True)
	print()