\documentclass{scrartcl}
\usepackage[top=1in, bottom=1.25in, left=1.25in, right=1.25in]{geometry}
\usepackage{array}
\usepackage[utf8x]{inputenc}
\usepackage[ngerman]{babel}
\usepackage{algorithm2e}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{lmodern}
\usepackage{stmaryrd}
%\usepackage{kpfonts}
\usepackage{tikz}
\usepackage{mathtools}
\usepackage{url}
\usepackage{microtype}
\DeclarePairedDelimiter{\ceil}{\lceil}{\rceil}	%Runden
\usetikzlibrary{trees}	%Bäume
\usetikzlibrary{positioning,calc,shapes,chains,arrows}

\setlength{\parindent}{0em}
\newcommand{\vcenteredinclude}[1]{\begingroup
\setbox0=\hbox{#1}%
\parbox{\wd0}{\box0}\endgroup}
\newcolumntype{P}[1]{>{\raggedright\arraybackslash}p{#1}}

\tikzset{
itria/.style={
  draw,dashed,shape border uses incircle,
  isosceles triangle,shape border rotate=90,yshift=-1.5cm}}

\pagenumbering{gobble}
\def\kuller{\bigcirc}
\newcommand{\shouldequal}{\overset{!}{=}}

\pagestyle{plain}

\begin{document}
\begin{center}$ $
{\fontsize{24}{10} \selectfont
\textbf{Artificial Life}$ $\\
$ $\\
\underline{Sheet 7}
}\\
\vspace{2em}
\begin{minipage}[t]{0.49\textwidth}
{\fontsize{15}{20} \raggedright \selectfont
Group \textbf{A}\\
Wed, 8-10h\\
$ $\\
}
\end{minipage}
\begin{minipage}[t]{0.49\textwidth}
{\fontsize{15}{20} \raggedleft \selectfont 
Tobias Kremeyer\\
}\end{minipage}
\end{center}
%\vspace{1em}
%{\fontsize{15}{20} \selectfont \setlength\extrarowheight{10pt}
%\begin{tabular}{|c||c|c||c|c||c|c||c|c||c|c|}
%\hline
%\textbf{Punkte:} & A1 & \hspace{1.7em} & A2 & \hspace{1.7em} & A3 & \hspace{1.7em} & A4 & \hspace{1.7em} & $\Sigma$ & \hspace{2.4em} \\
%\hline
%\end{tabular}}

\section*{A47) 2-point-crossover}
A 2-point-crossover selects two parents A and B and two crossover points $C_1$ and $C_2$. I will assume that the point before the first value is a possible crossover point, too, but the point after the last value is none. Therefore let the first possible crossover point be enumerated as point 0 and the last possible crossover point as point 63 which yields $L=64$ possible crossover points. Also I will assume $C_1 \neq C_2$. Therefore, a trivial crossover that would yield exactly the parent vectors as children is not possible.

Here we can apply the urn model with unordered samples without replacement (Combination) since we draw 2 different crossover points randomly. Therefore, the number of possibilities for choosing the crossover points is $\dbinom{L}{2} = \dbinom{64}{2} = 2016$. Since we produce 2 child vectors for each crossover point combination, this yields $2016 \cdot 2 = 4032$ possible children.

However, we need to consider the possibility of equal values in the parent vectors A and B. Let there be $e$ positions where the values of the respective field in A and B are equal. Then, for each of those positions, we don't care if a crossover happens before or after the position and can assume one less field in the vector. But, since in this case it is possible to generate the parent elements (set crossover points directly in front of and after the equal field), we need to add 2 more possible children. Therefore, the number of different possible children is
$$2\binom{L-e}{2}+2sign(e)$$

\section*{A48) Genomes and Hypercubes}
Each $d$-dimensional binary genome can be expressed as a corner of a $d$-dimensional axis-parallel hypercube with edge length 1 and each corner of this hypercube refers to a different binary genome. The coordinate vector of a corner just match the genome vector. A bit-flip at position $i$ then refers to moving along the $i$-th axis to the other corner of the hypercube. Please find a sketch in the appendix.

\section*{A49) TSP with EA}
\textbf{Given:} L cities that should each be visited exactly once. Genome structured as a sequence of these L cities. Mutation Operator: Swap two cities in the list.\\


By this mutation technique every possible sequence of cities can be reached. To reach a sequence $c_1c_2\dots c_L$, you can use any sorting algorithm that uses swaps, like Insertionsort.

\section*{A50) Probabilities for EA}
\textbf{Given:} Parent individual $X(i)$, genome of $L$ bit, $N$ offspring $X(i) = Y(i)_n$.\\
New generation $Y(i+1)_n$: Flip each bit with probability $p$.\\
$Q$: Probability for the case that none of the $N$ new individuals $Y(i+1)_n$ is identical to $X(i)$.

Let $\bar{Q}$ be the probability that a single new individual is not identical to $X(i)$. A single individual is identical to $X(i)$, if no bit is flipped. A single bit is not flipped with a probability of $q=1-p$, therefore $L$ bits are not flipped with a probability of $q^L$. This yields $\bar{Q} = 1-q^L$.\\
Since we need all $N$ individuals each not to be identical to $X(i)$, we can calculate:\\
$Q = \bar{Q}^N = (1-q^L)^N = (1-(1-p)^L)^N$\\

\underline{\textbf{Example:} $N=20, L=100, p=0.01$}\\
For this case we have the probability \\
$Q = (1-(1-p)^L)^N = (1-(1-0.01)^{100})^{20} \approx 1.1\times 10^{-4} = 0.00011 = 0.011\%$

\section*{A51) External vs Parent Selection}
The steps of external and parent selection differ in their purpose: \\
The external selection has the goal to select the best-fitting individuals out of the pool of individuals and discard all others. The questions are: \textbf{How many} of the individuals shall survive? \textbf{Which} are the ones to keep? The selection of individuals is mostly dependent of the individual fitness values which were evaluated before. To decrease the selection pressure a bit (Exploration of the search domain), the selection may be stochastic.\\
The pool that results from the external selection is then given to the parent selection, which in contrast to the latter has the goal to select parents for mating and recombination. In contrast to the external selection, individuals which have not been selected as parents stay in the pool of individuals, but are not allowed to reproduce for the iteration.

\section*{A52) Two's complement}
The two's complement value of a 32-bit string $b_{31}b_{30}\dots b_0$ is defined as follows:\\
$val(b_{31}b_{30}\dots b_0) = -b_{31}2^{31} + \sum\limits_{i=0}^{30}b_i2^i$\\
Therefore, as obvious in this formula, flipping bit $i$ adds or subtracts $2^i$ to/from the value of the number. The minimal difference (flip bit 0) is therefore $2^0=1$, the maximal difference (flip bit 31) is $2^{31}=2147483648$. The average difference is \\
$\dfrac{1}{32}\sum\limits_{i=0}^{31}2^i = \dfrac{2^{32}-1}{32} = 134217727.969$

\section*{A53) Industrial Application}
Genetic Algorithms are for example used for solving the timetable problem. This is a multi-constrained, NP-hard, combinatorial optimization problem which aims to create a university timetable and considers limited resources regarding teachers, classes, teaching hours, curricula and external conditions.

Colorni, Alberto \& Dorigo, Marco \& Maniezzo, Vittorio. (1994). A Genetic Algorithm To Solve The Timetable Problem. \url{https://www.researchgate.net/publication/2253354_A_Genetic_Algorithm_To_Solve_The_Timetable_Problem}

\end{document}