#include <iostream>
#include <fstream>
#include <list>
#include <cstdlib>
#include <string>

using namespace std;

class PredatorPreyPAC
{
private:
	double x,y;             //State variables
	double a,b,c,d,e,f,g,h; //Parameters
public:
	PredatorPreyPAC(double x0, double y0, double a, double b, double c, double d, double e, double f, double g, double h)
	: x(x0), y(y0), a(a), b(b), c(c), d(d), e(e), f(f), g(g), h(h) {}
	
	//Calculate one iteration
	void operator()()
	{
		double x_old = x;
		double y_old = y;
		
		x = x_old*(1 + a + e*x_old + g*y_old) + b*y_old;
		y = y_old*(1 + d + f*y_old + h*x_old) + c*x_old;
	}
	
	double getX(){return x;}
	double getY(){return y;}
	
	//Print x and y to some ostream
	void printXY(std::ostream& out)
	{
		out << x << "\t" << y << endl;
	}
};

void usage_notice()
{
	cout << "Usage: ./PA-C csv_filename temporal_plot_filename phase_plot_filename x0 y0 a b c d e f [g h]" << endl;
}

int main(int argc, char **argv)
{
    //Read parameters
	if(argc != 12 && argc != 14)
	{
		usage_notice();
		return 1;
	}
	
	double x0,y0,a,b,c,d,e,f,g,h;
	x0 = atof(argv[4]);
	y0 = atof(argv[5]);
	a = atof(argv[6]);
	b = atof(argv[7]);
	c = atof(argv[8]);
	d = atof(argv[9]);
	e = atof(argv[10]);
	f = atof(argv[11]);
	if(argc == 14)
	{
		g = atof(argv[12]);
		h = atof(argv[13]);
	}
	else g=h=0;
	
	cout << "a=" << a << " b=" << b << " c=" << c << " d=" << d << " e=" << e << " f=" << f << " g=" << g << " h=" << h << endl;
	
	//Create Predator Prey System with given parameters
	PredatorPreyPAC pp(x0,y0,a,b,c,d,e,f,g,h);
	
	//Stats file
	ofstream outfile;
	outfile.open(argv[1]);
	
	//Perform iterations and print XY each time
	for(int i = 0; i < 10000; i++)
	{
		pp();
		pp.printXY(outfile);
	}
	
	outfile.close();
	
	//Plot results
	system((string("gnuplot -c temporal.gnuplot ") + argv[1] + " " + argv[2]).c_str());
	system((string("gnuplot -c phase.gnuplot ") + argv[1] + " " + argv[3]).c_str());
	
	return 0;
}