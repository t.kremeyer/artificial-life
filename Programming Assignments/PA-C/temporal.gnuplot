set terminal png size 1000, 800
set output ARG2
set ylabel 'Population'
set xlabel 'Iteration'
set grid
plot ARG1 using 0:1 with lines lc rgb 'red'  title 'x(i)', ARG1 using 0:2 with lines lc rgb 'blue' title 'y(i)'
