set terminal png size 1000, 800
set output ARG2
set ylabel 'y'
set xlabel 'x'
set grid
plot ARG1 using 1:2 with lines lc rgb 'red' title 'Phase'
