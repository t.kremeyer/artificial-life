# PA-C: Predator-Prey

## Requirements

g++ for compilation and gnuplot for the plots

## Compile
Clone the project and execute `make`. 

## Run
Run the program using parameters
```
./PA-C tsv_filename temporal_plot_filename phase_plot_filename x0 y0 a b c d e f [g h]
```
tsv_filename: Name of a TSV file to which the values of x(i) and y(i) are saved  
temporal_plot_filename: Name of a PNG file to save the temporal plot to  
phase_plot_filename: Name of a PNG file to save the phase plot to  

The parameters g and h are optional. If not specified, they are set to 0

Example:
```
./PA-C out.tsv temporal.png phase.png 1 0 -0.040507026 -0.281732557 0.281732557 -0.040507026 0.1 0
```

The program always executes 10000 steps and afterwards calls gnuplot internally to plot the results. 

## Oscillation
The values given in the above example yield an almost stable oscillation.

## Example plots
The plots `temporal.png` and `phase.png` show the development of a stable oscillation with parameters:
a=-0.040507026, b=-0.281732557, c=0.281732557, d=-0.040507026, e=0, f=0