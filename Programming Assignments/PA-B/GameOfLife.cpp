#include "GameOfLife.hpp"

//Implementation of modulo that always outputs non-negative number
//This implements torus topology
int mod(int a, int b)
{
	//Add b in case a%b is negative to make it positive. Then for already positive numbers calculate mod b once more
	return (b+(a%b)) % b;
}