# PA-B Artificial Life: Game Of Life

## Compile
Clone the project and execute `make`. The Makefile uses g++ for compilation

## Run
Run the program using parameters
```
./PA-B <Init Pattern> <Filename for stats> <Number of Timesteps>
```
Possible Init Patterns:
- blinker
- glider
- r-pentomino
- glidergun
- acorn

Filename for stats: Name of the file that the active cell counts are saved to  
Number of Timesteps: The number of timesteps to perform  

Example:
```
./PA-B acorn stats.csv 1000
```
The program prints the resulting Game of Life configurations to stdout (On = "X", Off = "-")  
I would recommend to save stdout to a file like this:
```
./PA-B acorn acorn_stats.csv 1000 > acorn.txt
```
This way you don't need to scroll back to review the results.
