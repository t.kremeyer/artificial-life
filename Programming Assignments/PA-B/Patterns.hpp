/*
 * This file introduces some example init patterns for Game of Life.
 */

#ifndef PATTERNS_HPP
#define PATTERNS_HPP

#include "GameOfLife.hpp"


void blinker(GameOfLife &ca);

void glider(GameOfLife &ca);

void r_pentomino(GameOfLife &ca);

void glidergun(GameOfLife &ca);

void acorn(GameOfLife &ca);

#endif