#include "Patterns.hpp"

void blinker(GameOfLife &ca)
{
	ca(50,39) = 1;
	ca(50,40) = 1;
	ca(50,41) = 1;
}

void glider(GameOfLife &ca)
{
	ca(0,2) = 1;
	ca(1,2) = 1;
	ca(2,2) = 1;
	ca(2,1) = 1;
	ca(1,0) = 1;
}

void r_pentomino(GameOfLife &ca)
{
	ca(50,39) = 1;
	ca(51,39) = 1;
	ca(49,40) = 1;
	ca(50,40) = 1;
	ca(50,41) = 1;
}

void glidergun(GameOfLife &ca)
{
	ca(1,5) = 1;
	ca(1,6) = 1;
	ca(2,5) = 1;
	ca(2,6) = 1;
	ca(11,5) = 1;
	ca(11,6) = 1;
	ca(11,7) = 1;
	ca(12,4) = 1;
	ca(12,8) = 1;
	ca(13,3) = 1;
	ca(13,9) = 1;
	ca(14,3) = 1;
	ca(14,9) = 1;
	ca(15,6) = 1;
	ca(16,4) = 1;
	ca(16,8) = 1;
	ca(17,5) = 1;
	ca(17,6) = 1;
	ca(17,7) = 1;
	ca(18,6) = 1;
	ca(21,3) = 1;
	ca(21,4) = 1;
	ca(21,5) = 1;
	ca(22,3) = 1;
	ca(22,4) = 1;
	ca(22,5) = 1;
	ca(23,2) = 1;
	ca(23,6) = 1;
	ca(25,1) = 1;
	ca(25,2) = 1;
	ca(25,6) = 1;
	ca(25,7) = 1;
	ca(35,3) = 1;
	ca(35,4) = 1;
	ca(36,3) = 1;
	ca(36,4) = 1;
}

void acorn(GameOfLife &ca)
{
	ca(70,40) = 1;
	ca(71,38) = 1;
	ca(71,40) = 1;
	ca(73,39) = 1;
	ca(74,40) = 1;
	ca(75,40) = 1;
	ca(76,40) = 1;
}