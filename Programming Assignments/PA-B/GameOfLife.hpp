#ifndef GAME_OF_LIFE_HPP
#define GAME_OF_LIFE_HPP

#include <iostream>
#include <bitset>

#define CA_WIDTH 101
#define CA_HEIGHT 82

using namespace std;

//Implementation of modulo that always outputs non-negative number
//This implements torus topology
int mod(int a, int b);

class GameOfLife
{
public:

	//Get a reference to a given cell so it can be modified by users manually
	bitset<CA_WIDTH * CA_HEIGHT>::reference operator()(int x, int y){return getCell(x,y,activeCellset());}
	
	//Execute one time step (go to next generation)
	void step()
	{
		auto& active = activeCellset();
		auto& buffer = bufferCellset();
				
		//Loop through cells
		for(int x = 0; x < CA_WIDTH; x++)
		for(int y = 0; y < CA_HEIGHT; y++)
		{
			unsigned int activeNeighbours = 0;
			
			//For each cell: Loop through neighbourhood. Count active neighbours.
			for(int i = -1; i <= 1; i++)
			for(int j = -1; j <= 1; j++)
				//(i|j): Excludes the cell (x,y) itself
				//modulo to get torus topology
				if(getCell(mod(x+i, CA_WIDTH),mod(y+j, CA_HEIGHT),active)==1 && (i|j)) activeNeighbours++;
						
			//if 2 active neighbours: same content as before
			if(activeNeighbours == 2) 
			{
				getCell(x,y,buffer) = getCell(x,y,active);
			}
			//if 3 active neighbours: cell alive
			else if(activeNeighbours == 3)
			{
				getCell(x,y,buffer) = 1;
			}
			//else (1 or >=4): cell dead
			else
			{
				getCell(x,y,buffer) = 0;
			}
		}
		
		swapCellsets();
		timestep++;
	}
	
	//Print the state to given outstream, default to stdout
	void printState(char on = 'X', char off = '-', ostream &out = cout) const
	{
		auto active = activeCellset();
		
		out << "------------------------------------------- TIMESTEP " << timestep << " -------------------------------------------" << endl << endl;
		//Loop through rows and cols
		for(int y = 0; y < CA_HEIGHT; y++)
		{
			for(int x = 0; x < CA_WIDTH; x++)
			{
				if(getCell(x,y,active)) out << on;
				else out << off;
			}
			out << endl;
		}
		out << "=====================================================================================================" << endl << endl;
	}
	
	//The number of active cells in the grid
	unsigned int activeCellCount() const
	{
		auto& active = activeCellset();

		int count = 0;
		
		//Loop through cells and count
		for(int x = 0; x < CA_WIDTH; x++)
		for(int y = 0; y < CA_HEIGHT; y++)
		{
			if(getCell(x,y,active)) count++;
		}
		
		return count;
	}
	
	//Get current timestep of CA
	unsigned int getTimestep() const {return timestep;}
	
private:
	bitset<CA_WIDTH * CA_HEIGHT> cellset0;	//Two alternating cellsets (at each time: one active, one buffer for changes)
	bitset<CA_WIDTH * CA_HEIGHT> cellset1;
	char activeCellsetNumber = 0;			//Indicator for which cellset is active and which one works as buffer
	unsigned int timestep = 0;				//Timestep just for information
	
	//Get reference to the active cell set
	bitset<CA_WIDTH * CA_HEIGHT>& activeCellset()
	{
		if(activeCellsetNumber == 0) return cellset0;
		else return cellset1;
	}
	
	//Get reference to the buffer cell set
	bitset<CA_WIDTH * CA_HEIGHT>& bufferCellset()
	{
		if(activeCellsetNumber == 0) return cellset1;
		else return cellset0;
	}
	
	//Get reference to the active cell set (const version)
	const bitset<CA_WIDTH * CA_HEIGHT>& activeCellset() const
	{
		if(activeCellsetNumber == 0) return cellset0;
		else return cellset1;
	}
	
	//Get reference to the buffer cell set (const version)
	const bitset<CA_WIDTH * CA_HEIGHT>& bufferCellset() const
	{
		if(activeCellsetNumber == 0) return cellset1;
		else return cellset0;
	}
	
	//Set active cell set as buffer and the other way
	void swapCellsets()
	{
		if(activeCellsetNumber == 0) activeCellsetNumber = 1;
		else activeCellsetNumber = 0;
	}
	
	//Get reference to a cell of a given cellset. 
	//Cellset is organized such that cell x,y is at position x*height+y
	static bitset<CA_WIDTH * CA_HEIGHT>::reference getCell(int x, int y, bitset<CA_WIDTH * CA_HEIGHT> &cellset) 
	{
		return cellset[x*CA_HEIGHT + y];
	}
	
	//Get content of a cell of a given cellset. 
	//Cellset is organized such that cell x,y is at position x*height+y
	static const bool getCell(int x, int y, const bitset<CA_WIDTH * CA_HEIGHT> &cellset)
	{
		return cellset[x*CA_HEIGHT + y];
	}
};

#endif