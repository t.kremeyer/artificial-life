#include <cstring>
#include "Patterns.hpp"
#include "GameOfLife.hpp"
#include <fstream> 

using namespace std;

void usageMessage()
{
	cerr << "Usage: ./PA-B <Init Pattern> <Filename for stats> <Number of Timesteps>" << endl;
	cerr << "Possible Init Patterns:" << endl;
	cerr << "blinker" << endl << "glider" << endl << "r-pentomino" << endl << "glidergun" << endl << "acorn" << endl;
}

int main(int argc, char **argv)
{
	if(argc < 4)
	{
		usageMessage();
		return 1;
	}
	
	GameOfLife ca = GameOfLife();
	
	//Init Grid
	if(strcmp(argv[1], "blinker") == 0)
		blinker(ca);
	else if(strcmp(argv[1], "glider") == 0)
		glider(ca);
	else if(strcmp(argv[1], "r-pentomino") == 0)
		r_pentomino(ca);
	else if(strcmp(argv[1], "glidergun") == 0)
		glidergun(ca);
	else if(strcmp(argv[1], "acorn") == 0)
		acorn(ca);
	else
	{
		usageMessage();
		return 2;
	}
	
	//Stats file
	ofstream outfile;
	outfile.open(argv[2]);
	
	int numTimesteps = atoi(argv[3]);
	
	clog << "Program running." << endl;
	
	//Print initial state
	ca.printState();
	outfile << ca.activeCellCount() << endl;
	
	//Execute as many steps as requested and print them
	for(int i = 0; i < numTimesteps; i++)
	{
		ca.step();
		ca.printState();
		outfile << ca.activeCellCount() << endl;
		if(ca.getTimestep() % 50 == 0)
			clog << "Steps performed: " << ca.getTimestep() << endl;
	}
	
	outfile.close();
}