# PA-E: Evolutionary Algorithms

## Requirements

g++ for compilation

## Compile
Clone the project and execute `make`. 

## Run
Run the program
```
./PA-E
```

There are no parameters; the example is hardcoded for now.

## Example
The alleles of the starting genomes in the example code are each randomly, uniformly distributed in [-1000,1000]. The example fitness function searches the point (42,42,42,42) in R^4. Therefore, the fitness is calculated as  
`1/(Manhattan distance between genome and (42,42,42,42)).`  
Offspring genomes are randomly modified by adding random values distributed normally with mean=0, stddev=6.6. The algorithm stops if either 100000 steps are performed or a fitness of at least 1.0 is reached (i.e. a Manhattan distance at most 1)

For each step, the program outputs the step number and the genomes with their fitness values. First, in brackets the fitness value is written, following are the tab-delimited values of the alleles.

Own fitness functions could be implemented and passed to the EA class on initialization. Also, the random distributions can be set arbitrarily. That way, the EA class is able to be executed for a lot of EA problems.