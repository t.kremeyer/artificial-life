#ifndef _RANDOM_HPP_
#define _RANDOM_HPP_

#include <random>

using namespace std;

//A class that generates random doubles which we need
class RNG
{
private:
	random_device rd;
	mt19937 gen;
	
public:
	RNG() : gen{rd()} {}

	double randDoubleUniform(double min, double max)
	{
		return uniform_real_distribution<>{min,max}(gen);
	}
	double randDoubleNormal(double mean, double stddev)
	{
		return normal_distribution<>{mean, stddev}(gen);
	}
};

#endif