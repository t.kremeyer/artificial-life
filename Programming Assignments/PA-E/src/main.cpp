#include <iostream>
#include <vector>
#include <functional>
#include <cstdlib>
#include <iomanip>

#include "Random.hpp"

using namespace std;

typedef vector<double> Genome;

// Prints one genome tab-separated to an outstream
void print_genome(const Genome &genome, ostream &out = cout)
{
	for(double a:genome)
	{
		out << a << "\t";
	}
}

// An EA represents an evolutionary algorithm with its fitness function and 
// its current state, including population, fitness, offspring and 
// stopping criteria. 
class EA
{
private:
	
	//Initialization parameters
	bool uniform_distribution = true;	//Using normal or else normal distribution?
	//Distribution parameters
	double epsilon = 1.0;
	double mean = 0.0;
	double stddev = 1.0;

	function<double(Genome)> fitness_function;	//The fitness function to use

	//State
	vector<Genome> population;			//Current population
	vector<double> fitness;				//Current fitness of population
	vector<Genome> offspring;			//Current offspring
	int iteration = 0;					//Current iteration
	
	//Stopping criteria
	const int max_iterations;			//Maximum number of iterations to perform
	const double fitness_threshold;		//At what fitness should we stop?
	
	//Random generator
	RNG& rng;
	
	// Execute fitness function for every genome in the population
	// and store the fitness results
	void evaluate_fitness()
	{
		fitness.clear();
		for(Genome &g : population)
		{
			fitness.push_back(fitness_function(g));
		}
	}
	
	// External Selection
	// Set population to the current best genome
	void select_external()
	{
		Genome winner = _get_winner();
		population.clear();
		population.push_back(winner);
	}
	
	// Check stopping criteria, return true if met, else false
	bool done() const
	{
		if(max_iterations > 0 && iteration > max_iterations) return true;
		for(double f : fitness) if(f > fitness_threshold) return true;
		return false;
	}
	
	// Parent selection
	// Currently does nothing
	void select_parents() {}
	
	// Inheritance
	// Double the current genome
	void inherit()
	{
		offspring.clear();
		offspring.push_back(Genome(population[0]));
	}
	
	// Mutation
	// Mutate the current offspring genome and put it into the population
	void mutate()
	{
		for(Genome &g : offspring)
		{
			// Mutate randomly
			for(double &a : g)
			{
				a+= uniform_distribution?rng.randDoubleUniform(-epsilon, epsilon)
					: rng.randDoubleNormal(mean,stddev);
			}
			population.push_back(g);
		}
	}
	
	// Get the current winner (genome with the highest fitness)
	Genome &_get_winner()
	{
		int best = 0;
		double best_fitness = fitness[0];
		for(unsigned int i = 1; i < fitness.size(); i++)
		{
			if(fitness[i] > best_fitness)
			{
				best = i;
				best_fitness = fitness[i];
			}
		}
		
		return population[best];
	}
	
public:
	// Create an EA
	// fitness_function: The fitness function to use
	// init_population: The population to start with
	// max_iterations: Max number of iterations to perform (if not positive, is not applied)
	// fitness_threshold: The fitness that should be reached in order to stop
	// rng: The Random Number Generator to use
	EA(function<double(Genome)> fitness_function, vector<Genome>& init_population, int max_iterations, double fitness_threshold, RNG& rng) : fitness_function(fitness_function), population{init_population}, max_iterations(max_iterations), fitness_threshold(fitness_threshold), rng(rng)
	{
		evaluate_fitness();
	}
	
	// Execute one EA step
	bool operator()()
	{
		select_external();
		if(done()) return true;
		select_parents();
		inherit();
		mutate();
		evaluate_fitness();
		return false;
	}
	
	// use normal distribution for mutating the new genomes
	void set_normal_distribution(double _mean, double _stddev)
	{
		uniform_distribution = false;
		mean = _mean;
		stddev = _stddev;
	}
	
	// use uniform distribution for mutating the new genomes
	void set_uniform_distribution(double _epsilon)
	{
		uniform_distribution = true;
		epsilon = _epsilon;
	}
	
	// print the population and its fitness to out
	// Format per genome with alleles a1,a2,a3,a4 and fitness f:
	// [f]	a1	a2	a3	a4
	void print_population_and_fitness(ostream &out = cout) const
	{
		for(unsigned int i = 0; i < population.size(); i++)
		{
			out << '[' << setw(9) << fitness[i] << "]\t";
			print_genome(population[i], out);
			out << endl;
		}
		out << endl;
	}
	
	// Returnes the current best genome
	const Genome &get_winner()
	{
		return _get_winner();
	}
};

int main()
{
	RNG rng;
	vector<Genome> init_genomes;
	// Randomize init genomes
	for(int i = 0; i < 2; i++)
	{
		Genome g{rng.randDoubleUniform(-1000, 1000), rng.randDoubleUniform(-1000, 1000), rng.randDoubleUniform(-1000, 1000), rng.randDoubleUniform(-1000, 1000)};
		init_genomes.push_back(g);
	}
	
	cout << "Init Genomes:" << endl;
	for(Genome &g : init_genomes)
	{
		print_genome(g);
		cout << endl;
	}
	cout << endl;
	
	// Initialize EA
	// Use a fitness function that yields the fitness 
	// 1/(Manhattan distance from the genome to the point (42,42,42,42))
	// 
	// Not more than 100000 steps
	// Fitness to reach: 1.0
	EA ea([](Genome genome) -> double{
		double fitness = 0;
		static Genome target {42, 42, 42, 42};
		//Fitness = 1/Manhattan Distance to target point
		for(unsigned int i = 0; i < genome.size(); i++)
		{
			fitness += abs(target[i]-genome[i]);
		}
		
		fitness = 1/fitness;
		return fitness;
	}, init_genomes, 100000, 1.0, rng);
	// Use a normal distribution mean=0, stddev=6.6 to mutate genomes
	ea.set_normal_distribution(0.0, 6.6);
	
	unsigned int i = 0;
	
	// Execute EA until done. Print info each time
	do
	{
		cout << " --- Step " << i << "---" << endl;
		ea.print_population_and_fitness();
		i++;
	}while(!ea());
	
	// Print winner
	const Genome &w = ea.get_winner();
	cout << "WINNER: ";
	print_genome(w);
}	