# PA-A Artificial Life

## Compile
Clone the project and execute `make`. The Makefile uses g++ for compilation

## Run
Run the program using parameters
```
./PA-A <Neighbourhood Radius> <Wolfram Rule> <Starting Condition (S: Seed, R: Random) [default: S]>
```
Example:
```
./PA-A 1 30 S
```
The program prints the resulting CA to stdout.
I would recommend to save stdout to a file like this:
```
./PA-A 1 30 S >> out_1_30_S.txt
```
This way you don't need to scroll back to review the results.

I decided to not let the user enter the CA parameters runtime via stdin but via console parameters so 
it's possible to save the results to a file this way.
