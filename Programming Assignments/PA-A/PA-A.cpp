#include <iostream>
#include <bitset>
#include <string>
#include <random>
#include <cstring>

using namespace std;

//Only use 80 cells since on the left and right border there are 2 read-only "0" fields which is modeled by the CA class separately. The class uses 2 sets of cells of which at every time one is active and the other is used as a buffer for changes.
#define CELL_COUNT 80

//Class for the cellular automaton. The CA considers out-of-bounds-fields as "0" and readonly. 
class CA
{
public:
	CA(unsigned int neighbourhoodRadius, unsigned int rule) 
		: neighbourhoodRadius(neighbourhoodRadius), rule(rule)
	{}
	
	//Execute one time step
	void step()
	{
		//Loop through the cells, calculate next state and write result to buffer
		for(int j = 0; j < CELL_COUNT; j++)
		{
			bufferCells()[j] = nextState(rule, cellNeighbourhood(j));
		}
		//Set buffer as active cells
		swapCellsets();
	}
	
	//Print state. Append two 0s at the front and end as required by the assignment
	void printState()
	{
		cout << "00";
		for(int j = 0; j < CELL_COUNT; j++)
		{
			cout << activeCells()[j];
		}
		cout << "00" << endl;
	}
	
	//Randomize the state of all cells using a prob. of 0.5 for resp. "1" or "0"
	void randomizeState()
	{
		//Random initialization
		random_device rd;
		mt19937 gen(rd());
		uniform_int_distribution<> dist(0,1);
		
		//Randomize each cell
		for(int j = 0; j < CELL_COUNT; j++)
		{
			if(dist(gen)) activeCells().set(j);
			else activeCells().reset(j);
		}
	}
	
	//Get a reference to a given cell so it can be modified by users manually
	bitset<CELL_COUNT>::reference operator[](int j){return activeCells()[j];}
	
private:
	bitset<CELL_COUNT> cells0;				//Two stores for the cells (used in an alternating way).
	bitset<CELL_COUNT> cells1;				//One acts as the active one and the other as the buffer for a new step
											//Initialized to all "0"

	int activeCellsNumber;					//Indicator for the active cellset
	const unsigned int neighbourhoodRadius;
	unsigned int rule;						//Wolfram number of rule
	
	
	//Get the neighbourhood of cell j as integer style bitfield (binary representation of this integer is the neighbourhood)
	unsigned int cellNeighbourhood(int j)
	{
		unsigned int neighbourhood = 0;
		
		//Loop through neighbourhood fields (from left-most to right-most)
		for(int i = -(int)neighbourhoodRadius; i <= 0+(int)neighbourhoodRadius; i++)
		{
			//We only need to do something if cell is in bounds and set to 1
			//Else we would need to write a 0, but neighbourhood is already initialized to all 0s
			if(j+i >= 0 && j+i < CELL_COUNT && activeCells()[j+i])
			{
				//Write a 1 into neighbourhood to the considered position
				neighbourhood |= (0b1 << (neighbourhoodRadius-i));
			}
		}
		
		return neighbourhood;
	}
	
	//Calculate the next state of some cell with given neighbourhood for given rule
	static bool nextState(unsigned int rule, unsigned int neighbourhood)
	{
		//Extract the bit out of the rule that fits the neighbourhood
		//c.f. definition of Wolfram number
		return (rule >> neighbourhood) & 0b1;
	}
	
	//Get reference to the active cell set
	bitset<CELL_COUNT>& activeCells()
	{
		if(activeCellsNumber == 0) return cells0;
		else return cells1;
	}
	
	//Get reference to the buffer cell set
	bitset<CELL_COUNT>& bufferCells()
	{
		if(activeCellsNumber == 0) return cells1;
		else return cells0;
	}
	
	//Set active cell set as buffer and the other way
	void swapCellsets()
	{
		if(activeCellsNumber == 0) activeCellsNumber = 1;
		else activeCellsNumber = 0;
	}
};

int main(int argc, char **argv)
{	
	//Check and parse arguments
	if(argc < 4)
	{
		cout << "Usage: ./PA-A <Neighbourhood Radius> <Wolfram Rule> <Starting Condition (S: Seed, R: Random) [default: S]>" << endl;
		return 1;
	}
	
	string input;
	int neighbourhoodRadius = atoi(argv[1]);
	unsigned int rule = atoi(argv[2]);
	
	/*cout << "Neighbourhood radius: ";
	cin >> input;
	neighbourhoodRadius = stoi(input);
	
	cout << "Wolfram Rule: ";
	cin >> input;
	rule = stoi(input);
	
	cout << "Starting Condition (S: Seed, R: Random)[default: S]: ";
	cin >> input; */
	
	//Create CA
	CA ca(neighbourhoodRadius, rule);

	//Check if condition "R" or "S" (default)
	if(strcmp("R",argv[3])==0) ca.randomizeState();	
	else ca[40] = 1;	//The two "0"s at the left and right for us don't count as cells, so set cell 40 instead of 42 to "1" for condition "Seed".
	
	ca.printState();	//Print start state
	
	clog << "Program running. Press CTRL+C to stop" << endl;
	
	//Execution of CA
	while(true)
	{
		ca.step();
		ca.printState();
	}
	
}